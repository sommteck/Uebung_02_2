using System;

// 1: Anlegen des Projektes

namespace Uebung_02_2_Basic
{
    class Program
    {
        static void Main(string[] args)
        {
            // 2: Deklarieren des Arrays für Zahlen 1 bis 7,
            //    der Wochentage Sonntag bis Samstag und
            //    des zweidimensionalen Arrays

            int[] tagIndex;
            tagIndex = new int[7] { 1, 2, 3, 4, 5, 6, 7 };

            string[] wochenTage = new string[7] { "Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag" };

            string[,] endArray = new string [2, 7];

            // 3: Übertragen der beiden eindimensionalen Arrays
            //    in das zweidimensionale Array mit Typumwandlung

            /*
            for (int i=0; i < 7; i++)
            {
                endArray[0, i] = Convert.ToString(tagIndex[i]);
                endArray[1, i] = wochenTage[i];
            }
            */

            for (int i = 0; i < 7; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if (j == 0) endArray[j, i] = Convert.ToString(tagIndex[i]);
                    else endArray[j, i] = wochenTage[i];
                }
            }

            // 4: Ausgabe des zweidimensionalen Arrays nach Vorgabe

            for (int i = 0; i < 7; i++)
            {
                // Console.WriteLine("{0} - {1}", endArray[0,i], endArray[1, i]);
                //
                /*
                Console.Write(endArray[0, i]);
                Console.Write(" - ");
                Console.WriteLine(endArray[1, i]);
                */
                Console.WriteLine(endArray[0,i] + " - " + endArray[1, i]);
                
            }

            Console.ReadKey();

            foreach (string str in endArray)
            {
                Console.WriteLine(str);
            }

            Console.ReadKey();
            
            
            
            
            // 5: Ausgabe des zweidimensionalen Arrays mit foreach

            bool wahr = true;
            foreach (string str in endArray)
            {
                if (wahr)
                {
                    Console.Write(str);
                    wahr = false;
                }
                else
                {
                    Console.WriteLine(" - ", str);
                    wahr = true;
                }
            }

            Console.ReadKey();

        }
    }
}